define([
  'rdfjson/Graph',
  'rdfjson/namespaces',
], (Graph, namespaces) => {
  const divider = (pre, post) => {
    console.log(
            `${pre ? '\n' : ''
            }  -------------------------------------------------------------${
            post ? '\n' : ''}`,
        );
  };

  const mesg = (message) => {
    console.log(` | ${message}`);
  };

  /**
   * @param {string|string[]|rdfjson/Graph} str1 either a string, an array of
   * strings or an RDF graph
   * @param {string|string[]} str2 if provided then the str1 is to be considered
   * an header (not applied when graph)
   */
  return (str1, str2) => {
    divider(true);
    if (str1 instanceof Graph) {
      str1.find().forEach((stmt) => {
        mesg(`< ${namespaces.shorten(stmt.getSubject())}, ${
                 namespaces.shorten(stmt.getPredicate())}, ${
                stmt.getType() === 'uri' ? namespaces.shorten(stmt.getValue()) : `"${stmt.getValue()}"`}>`);
      });
      divider(false, true);
      return;
    }
    if (typeof str2 === 'undefined') {
      (Array.isArray(str1) ? str1 : [str1]).forEach(mesg);
    } else {
      (Array.isArray(str1) ? str1 : [str1]).forEach(mesg);
      divider();
      (Array.isArray(str2) ? str2 : [str2]).forEach(mesg);
    }
    divider(false, true);
  };
});
