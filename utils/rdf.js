define([
  'dojo/Deferred',
  'dojo/node!fs',
  'rdfjson/Graph',
  'rdfjson/formats/converters',
  'store/EntryStoreUtil',
  'dojo/promise/all',
  'dojo/node!http',
], (Deferred, fs, Graph, converters, EntryStoreUtil, all, http) => {
  const rdf = {

    /**
     * It copies over statements from one a metadata graph to another.
     * The statements copied are those with the provided uri in subject position, or
     * statements reachable via intermediate blank nodes from that uri.
     * Statements are not copied over if the predicate is listed in the ignore array.
     *
     * @param {rdfjson/Graph} inGraph graph which holds rdf data in graph format
     * @param {string} uri a starting point to find all statements to include
     * @param {object=} ignore is an object with predicates as attributes,
     * which are to be ignored (excluded)
     * @param {rdfjson/Graph=} outGraph optional graph which holds copied statements
     * when constructing new outGraph, if no outGraph is provided a new will be created.
     * @return {rdfjson/Graph} the graph to which the statements has been copied
     */
    closure(inGraph, uri, ignore, outGraph) {
      const _outGraph = outGraph || new Graph();
      const _ignore = ignore || {};
      const stmts = inGraph.find(uri, null, null);
      for (let i = 0; i < stmts.length; i++) {
        const stmt = stmts[i];
        if (!_ignore[stmt.getPredicate()]) {
          _outGraph.add(stmts[i]);
          if (stmt.getType() === 'bnode') {
            rdf.closure(inGraph, _outGraph, stmt.getValue(), _ignore);
          }
        }
      }
      return _outGraph;
    },

    /**
     * Loads a file and converts into a graph
     * @param filePath - file path of rdf data to be imported
     * @returns {Deferred} - returns a promise with a Graph.
     */
    loadRDF(filePath) {
      try {
        const d = new Deferred();
        fs.readFile(filePath, 'utf8', (err, data) => {
          d.resolve(converters.rdfxml2graph(data));
        });
        return d; // returns a promise with a Graph.
      } catch (error) {
        console.log('Found error while reading file, check path');
      }
      return undefined;
    },

    loadJSON(url) {
      const d = new Deferred();
      http.get(url, (response) => {
        let body = '';
        response.on('data', (data) => {
          body += data;
        });
        response.on('end', () => {
          d.resolve(JSON.parse(body));
        });
      });
      return d;
    },

    /**
     * utility to load RDF from URL
     * @param url
     * @returns {Deferred}
     */
    loadRDFFromURL(url) {
      const d = new Deferred();
      http.get(url, (response) => {
        // Continuously update stream with data
        let body = '';
        response.on('data', (data) => {
          body += data;
        });
        response.on('end', () => {
          // Data reception is done, do whatever with it!
          const parsed = converters.rdfxml2graph(body);
          // console.log(parsed);
          d.resolve(parsed);
        });
      });
      return d; // returns a promise with a ckan json.
    },

    /**
     * Deletes all entries fo the specified types in a context.
     * (I.e. an entry is deleted if there is an rdf:type pointing to one of the
     * specified types in the metadata graph of the entry.)
     *
     * @param {store/Context} contextId
     * @param {string[]} types
     * @returns {promise} a promise indicating when the clearing is complete
     */
    clearContext(entrystore, contextId, types) {
      const context = entrystore.getContextById(contextId);
      const esu = new EntryStoreUtil(entrystore);
      return esu.removeAll(entrystore.newSolrQuery()
        .rdfType(types)
        .context(context)
        .limit(100)
        .list());
    },
  };
  return rdf;
});
