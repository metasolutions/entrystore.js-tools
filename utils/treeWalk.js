define(['./globals'], globals =>
  /**
   * @param params.onEach - a callback that will be called for each entry
   * @param params.forward - properties to children
   * @param params.backward - properties to parents
   * @param params.rootType - an rdf type to find the root node(s)
   * @param params.context - the context to limit the traversal in
   */
  (params) => {
    const passed = new Set();
    const es = globals.entrystore;
    const stack = [];
    const walk = async (node) => {
      if (!passed.has(node.getURI())) {
        passed.add(node.getURI());
        params.onEach(node, stack);

        const md = node.getMetadata();
        let leaf = true;
        params.forward.forEach((prop) => {
          if (md.findFirstValue(node.getResourceURI(), prop)) {
            leaf = false;
          }
        });
        if (!leaf) {
          const query = es.newSolrQuery();
          query.disjuntiveProperties();
          if (params.context) {
            query.context(params.context);
          }
          params.backward.forEach((back) => {
            query.uriProperty(back, node.getResourceURI());
          });
          stack.push(node);
          return query.list().forEach(walk).then(() => {
            stack.pop();
          });
        }
      }
    };
    const tq = globals.entrystore.newSolrQuery().rdfType(params.rootType);
    if (params.context) {
      tq.context(params.context);
    }
    return tq.list().forEach(walk);
  });
