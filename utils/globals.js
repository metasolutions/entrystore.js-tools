define([
  'config',
  'store/EntryStore',
  'store/EntryStoreUtil',
], (config, EntryStore, EntryStoreUtil) => {
  const entryStore = new EntryStore(config.repository);
  config.entrystore = entryStore;
  const entryStoreUtil = new EntryStoreUtil(entryStore);
  config.entrystoreutil = entryStoreUtil;

  return {
        /**
         * @type {store/EntryStore}
         */
    entrystore: entryStore,
        /**
         * @returns {store/EntryStoreUtil}
         */
    entrystoreutil: entryStoreUtil,
  };
});
