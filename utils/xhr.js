define([
  'dojo/Deferred',
  'rdfjson/formats/converters',
  'dojo/node!http',
  'dojo/node!https',
], (Deferred, converters, http, https) => {
  const xhr = {
    /**
     * Loads a file and converts into a graph
     * @param filePath - file path of rdf data to be imported
     * @returns {Deferred} - returns a promise with a Graph.
     */
    loadRDF(url) {
      return xhr.loadText(url).then(txt => converters.rdfxml2graph(txt));
    },

    loadJSON(url) {
      return xhr.loadText(url).then(txt => JSON.parse(txt));
    },

    loadText(url) {
      const d = new Deferred();
      const h = url.indexOf('https') === 0 ? https : http;
      h.get(url, (response) => {
        let body = '';
        response.on('data', (d) => {
          body += d;
        });
        response.on('end', () => {
          d.resolve(body);
        });
      });
      return d;
    },
  };
  return xhr;
});
