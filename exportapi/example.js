define([
  'config',
  'rdfjson/namespaces',
], (config, namespaces) => {
  namespaces.add('gn', 'http://www.geonames.org/ontology#');
  namespaces.add('wd', 'http://www.wikidata.org/prop/direct/');

  config.exportAsAPI = {
    port: 8182,
    country: {
      rdfType: 'gn:Feature',
      projection: {
        name: 'gn:name',
        iso1: 'wd:P299',
        iso2: 'wd:P297',
        iso3: 'wd:P298',
        seeAlso: 'rdfs:seeAlso',
        wikipedia: 'gn:wikipediaArticle',
      },
    },
    collection: {
      relation: 'dcterms:partOf',
      projection: {
        name: 'skos:prefLabel',
        parent: 'skos:broader',
      },
    },
    terminologycsv: {
      type: 'tree',
      forward: ['skos:narrower', 'skos:hasTopConcept'],
      backward: ['skos:broader', 'skos:topConceptOf'],
      rootType: 'skos:ConceptScheme',
    },
  };
  require(['store-tools/exportapi/service']);
});
