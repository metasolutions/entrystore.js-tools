/* global define*/
define([
  'dojo/json',
  'store-tools/utils/globals',
  'dojo/io-query',
  'config',
  'store-tools/utils/treeWalk',
  'dojo/node!http',
  'dojo/node!url',
], (json, globals, ioQuery, config, skosWalk, http, url) => {
  const entrystore = globals.entrystore;
  const auth = entrystore.getAuth();

  let authtime = new Date().getTime();
  const authenticate = function () {
    return auth.getUserInfo(true).then((ui) => {
      if (ui.id === '_guest') {
        return auth.login(config.user, config.password);
      }
      const newtime = new Date().getTime();
      if (newtime - authtime > 86400000) { // If last sign in was over 24 hours ago, sign in again.
        return auth.logout().then(() => {
          authtime = newtime;
          return auth.login(config.user, config.password);
        });
      }
      return Promise.resolve(true);
    });
  };

  const getExportConf = (request) => {
    let pathname = url.parse(request.url).pathname;
    if (pathname.length > 1) {
      pathname = pathname.substr(1);
      const idx = pathname.indexOf('/');
      if (idx !== -1) {
        pathname = pathname.substr(0, idx);
      }
      if (config.exportAsAPI[pathname]) {
        return config.exportAsAPI[pathname];
      } else if (config.exportAsAPI.default) {
        return config.exportAsAPI.default;
      }
      if (pathname.length > 0) {
        throw new Exception(`No configuration for path '${pathname}'`);
      } else {
        throw new Exception('No base configuration, you need to provide a path or the configuration of the export API is wrong.');
      }
    }
  };

  const getRequestParams = request =>
    ioQuery.queryToObject(url.parse(request.url).query || '');

  const getProjObj = (entry, conf) => {
    const md = entry.getMetadata();
    const obj = md.projection(entry.getResourceURI(), conf.projection);
    obj.contextId = entry.getContext().getId();
    obj.entryId = entry.getId();
    obj.entryuri = entry.getURI();
    obj.uri = entry.getResourceURI();
    return obj;
  };

  const handleRequest = (request, response) => authenticate().then(() => {
    const exportConf = getExportConf(request);
    const params = getRequestParams(request);
    const context = entrystore.getContextById(params.context);
    if (exportConf.type === 'tree') {
      response.setHeader('Content-Type', 'text/csv;charset=UTF-8');

      const label = (entry, top) => entry.getMetadata().findFirstValue(entry.getResourceURI(),
        (top ? 'dcterms:title' : 'skos:prefLabel'));
      const toPath = parents => parents.map((e, idx) =>
        label(e, idx === 0)).join(' > ');
      return skosWalk({
        context,
        forward: exportConf.forward,
        backward: exportConf.backward,
        rootType: exportConf.rootType,
        onEach: (entry, parents) => {
          response.write(`${entry.getId()},${label(entry, parents.length === 0)},${toPath(parents)}\n`);
        },
      });
    }
    const result = [];
    let query = entrystore.newSolrQuery().context(context);
    if (exportConf.constraints) {
      for (const key in exportConf.constraints) {
        if (exportConf.constraints) {
          query = query.uriProperty(key, exportConf.constraints[key]);
        }
      }
    } else if (exportConf.rdfType) {
      query = query.rdfType(exportConf.rdfType);
    } else if (exportConf.relation) {
      const toResourceURI = entrystore.getResourceURI(params.context, params.entry);
      query = query.uriProperty(exportConf.relation, toResourceURI);
    } else {
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.write('[]');
      return Promise.resolve(true);
    }
    return query.list().forEach((fEntry) => {
      result.push(getProjObj(fEntry, exportConf));
    }).then(() => {
      response.setHeader('Content-Type', 'application/json;charset=UTF-8');
      response.write(json.stringify(result, null, '  '));
    });
  });

  http.createServer((req, res) => {
    if (req.method !== 'GET') {
      res.writeHead(405, { 'Content-Type': 'text/html' });
      res.write('<html><body><h1>Error - status code 405</h1><p>Method not allowed, only GET supported!</p></body></html>');
      res.statusCode = 405;
      res.end();
    } else {
      req.on('end', () => {
        handleRequest(req, res).then(() => {
          res.statusCode = 200;
          res.end();
        }).then(null, (err) => {
          res.writeHead(400, { 'Content-Type': 'application/json' });
          res.write(`{"errors": true, "rdfError": "${err}, "stack": "${err.stack}"}`);
          res.statusCode = 400;
          res.end();
        });
      });
    }
    req.resume();
  }).listen(config.exportAsAPI.port, '0.0.0.0');
  console.log('node-static running at http://localhost:%d', config.exportAsAPI.port);
});
