const cmd = process.argv[2];
dojoConfig = { locale: 'en' };// to run validate, which requires locale
const requirejs = require('requirejs');

requirejs.config({
  nodeRequire: require,
  baseUrl: '../node_modules',
  packages: [
    {
      name: 'store-tools',
      location: '..',
    },
    {
      name: 'config',
      location: '..',
      main: 'config.js',
    },
    {
      name: 'md5',
      location: 'blueimp-md5/js',
      main: 'md5.min',
    },
    {
      name: 'text',
      location: 'requirejs-text',
      main: 'text',
    },
  ],
  map: {
    '*': {
      has: 'dojo/has', // Use dojos has module since it is more clever.
      'dojo/text': 'text', // Use require.js text module
            // Make sure i18n, dojo/i18n and di18n/i18n are all treated as a SINGLE module named i18n.
            // (We have mapped i18n to be the module provided in di18n/i18n, see paths above.)
      'dojo/i18n': 'i18n',
      'di18n/i18n': 'i18n',
    },
    'store/rest': {
      'dojo/request': 'dojo/request/node', // Force using node
    },
    'rdforms/template/bundleLoader': {
      'dojo/request': 'dojo/request/node',  // Force using node
    },
  },
  deps: ['store-tools/utils/fix', `store-tools/${cmd}`],
});
