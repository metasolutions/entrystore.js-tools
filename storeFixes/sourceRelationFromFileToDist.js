define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'rdfjson/namespaces',
  'config',
], (globals, message, namespaces, config) => {
  const es = globals.entrystore;
  namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');

  es.getAuth().login(config.user, config.password).then(() => {
    es.newSolrQuery().rdfType('dcat:Distribution').uriProperty('dcterms:source', '*').list().forEach((apiDistEntry) => {
      // Load what dcterms:source points to
      const apiDistMd = apiDistEntry.getMetadata();
      const sourceURI = apiDistMd.findFirstValue(apiDistEntry.getResourceURI(), 'dcterms:source');
      es.getEntry(es.getEntryURIFromURI(sourceURI)).then((sourceEntry) => {
        // Is it a fileEntry?
        if (sourceEntry.getMetadata().find(null, 'rdf:type', 'dcat:Distribution').length === 0) {
          // find the Distribution from the fileEntry
          es.newSolrQuery().rdfType('dcat:Distribution').uriProperty('dcat:downloadURL', sourceURI).limit(1).list().getEntries().then((distEntries) => {
            if (distEntries.length === 1) {
              apiDistMd.findAndRemove(null, 'dcterms:source');
              apiDistEntry.add('dcterms:source', distEntries[0].getResourceURI());
              apiDistEntry.commitMetadata();
            } else {
              console.log(`Something went really wrong... with entry: ${apiDistEntry.getURI()}`);
            }
          });
        }
      });
    });
  });
});
