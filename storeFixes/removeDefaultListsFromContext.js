define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'store/promiseUtil',
  'config',
], (globals, message, promiseUtil, config) => {
  /**
   * This script searches for all contexts in store and for each of them deletes any
   * default lists (defaultListsURISuffixes) if it finds any.
   */

  const GT_LIST = 'List';
  const GT_CONTEXT = 'Context';
  const es = globals.entrystore;
  const base = config.repository;

  const defaultListsURISuffixes = [
    '_unlisted',
    '_latest',
    '_comments',
    '_top',
    '_contacts',
    '_featured',
    '_feeds',
    '_trash',
    '_systemEntries',
  ];

  const deleteDefaultListsInContext = (context) => {
    const contextId = context.getId();
    const defaultListUris = new Set(
      defaultListsURISuffixes.map(uri => `${base}${contextId}/resource/${uri}`));

    const entries = [];
    return es.newSolrQuery().context(context).graphType(GT_LIST)
      .list()
      .forEach((listEntry) => {
        if (defaultListUris.has(listEntry.getResourceURI())) {
          entries.push(listEntry);
        }
      })
      .then(() => promiseUtil.forEach(entries, (entry) => {
        const uri = entry.getResourceURI();
        console.log(`Removing default entrylist ${uri} from context ${contextId}`);
        return entry.del().then(null, err =>
          console.log(`Oops! something went wrong : ${err.message}`));
      }));
  };
  const contexts = [];
  es.getAuth().login(config.user, config.password).then(() => {
    es.newSolrQuery().graphType(GT_CONTEXT).list().forEach((contextEntry) => {
      contexts.push(contextEntry);
    })
      .then(() => promiseUtil.forEach(contexts, c => deleteDefaultListsInContext(c)));
  });
});
