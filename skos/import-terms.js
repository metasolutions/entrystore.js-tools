define([
  'store/EntryStore',
  'store/promiseUtil',
  'store-tools/utils/message',
  'store-tools/utils/rdf',
  'store-tools/libs/rdfjson/utils',
  'rdfjson/Graph',
  'config',
], (EntryStore, promiseUtil, message, rdf, utils, Graph, config) => {
  // Input handling
  // eslint-disable-next-line no-unused-vars
  const [nodePath, initFile, commandName, importFileName, ...filters] = process.argv;

  if (importFileName === undefined) {
    message('You need to provide a file to import');
    process.exit();
  }

  // Main
  const es = new EntryStore(config.repository);

  // Updates the console rather than adding a new line
  const updateConsoleLine = (msg) => {
    process.stdout.write('\r\x1b[K');
    process.stdout.write(msg);
  };

  const includeURI = (uri, filtersArr) => filtersArr.some(filter => uri.match(filter) != null);

  const createEntryInContext = async (contextResource, entryURI, graph) => {
    const newEntry = contextResource
      .newLink(entryURI);

    newEntry
      .setMetadata(utils.extract(graph, new Graph(), entryURI));

    await newEntry.commit();
    return newEntry;
  };

  const fixURIs = ({ graph, conceptSchemeURI, homeContextId }) => {
    let _conceptSchemeURI = conceptSchemeURI;
    const params = {};

    let counter = 0;
    let onlyCounter = false;
    const ids = {};
    let conceptSchemeURIReplaced = false;
    params.newURIs = [];
    params.anythingReplaced = false;

    if (config.terms == null || config.terms.replace == null) {
      return graph;
    }
    const replace = config.terms.replace;

    let cstmts;
    if (replace.matchPredicate) {
      cstmts = graph.find(null, replace.matchPredicate);
      if (replace.matchDatatype) {
        cstmts = cstmts.filter(stmt => stmt.getDatatype() === replace.matchDatatype);
      }
    }
    if (cstmts.length === 0 && (replace.URIRegexp || replace.allURIs)) {
      cstmts = graph.find(null, 'rdf:type', 'skos:Concept');
      onlyCounter = true;
      if (replace.URIRegexp) {
        cstmts = cstmts.filter(cstmts, stmt => replace.URIRegexp.exec(stmt.getSubject()));
      }
    }

    const fromTo = {};
    cstmts.forEach(cstmts, (cstmt) => {
      const s = cstmt.getSubject();
      let id;
      if (onlyCounter) {
        counter += 1;
        id = `${id}concept_${counter}`;
      } else {
        id = cstmt.getValue();
        if (ids[id]) {
          counter += 1;
          id = `${id}_${counter}`;
        }
      }
      ids[id] = true;
      const nuri = es.getResourceURI(homeContextId, id);
      graph.replaceURI(s, nuri);
      fromTo[s] = nuri;
      params.newURIs[nuri] = { old: s, newId: id };
      params.anythingReplaced = true;
      if (_conceptSchemeURI === s) {
        conceptSchemeURIReplaced = true;
        _conceptSchemeURI = nuri;
      }
    });
    if (!conceptSchemeURIReplaced) {
      if (replace.URIRegexp &&
        (replace.URIRegexp.exec(_conceptSchemeURI) !== null || replace.allURIs)) {
        const newCSURI = es.getResourceURI(homeContextId, 'terminology');
        graph.replaceURI(_conceptSchemeURI, newCSURI);
        params.newURIs[newCSURI] = { old: _conceptSchemeURI, newId: 'terminology' };
        fromTo[_conceptSchemeURI] = newCSURI;
        _conceptSchemeURI = newCSURI;
      }
    }

    return graph;
  };

  const fixSymmetry = ({ graph, conceptSchemeURI, filterArr = filters }) => {
    const broader = {};
    let topConcepts = graph.find(conceptSchemeURI, 'skos:hasTopConcept') || [];
    topConcepts = topConcepts.filter(stmt => includeURI(stmt.getValue(), filterArr));

    topConcepts.forEach((stmt) => {
      broader[stmt.getValue()] = conceptSchemeURI;
      graph.add(stmt.getValue(), 'skos:topConceptOf', stmt.getSubject());
    });

    let cstmts = graph.find(null, 'rdf:type', 'skos:Concept') || [];
    cstmts = cstmts.filter(stmt => includeURI(stmt.getSubject(), filterArr));
    cstmts.forEach((cstmt) => {
      const conceptURI = cstmt.getSubject();
      graph.add(conceptURI, 'skos:inScheme', conceptSchemeURI);
      const broaderConcepts = graph.find(conceptURI, 'skos:broader') || [];

      broaderConcepts.forEach((stmt) => {
        broader[conceptURI] = stmt.getValue();
        graph.add(stmt.getValue(), 'skos:narrower', conceptURI);
      });

      const topConceptOf = graph.find(conceptURI, 'skos:topConceptOf') || [];
      topConceptOf.forEach((stmt) => {
        broader[conceptURI] = stmt.getValue();
        graph.add(stmt.getValue(), 'skos:hasTopConcept', stmt.getSubject());
      });
    });

    const counter = Object.keys(broader).length;

    if (counter < cstmts.length && (cstmts.length - counter) < 500) {
      cstmts.forEach((cstmt) => {
        const conceptURI = cstmt.getSubject();
        if (!broader[conceptURI]) {
          graph.add(conceptURI, 'skos:topConceptOf', conceptSchemeURI);
          graph.add(conceptSchemeURI, 'skos:hasTopConcept', conceptURI);
        }
      });
    }

    return graph;
  };

  const processGraph = ({ graph, conceptSchemeURI, homeContextID, filtersArr }) => fixSymmetry({
    graph: fixURIs({ graph, conceptSchemeURI, homeContextID }),
    conceptSchemeURI,
    filters: filtersArr,
  });

  // Load the file and start the import
  rdf.loadRDF(importFileName)
    .then(async (importedGraph) => {
      try {
        // Authorization
        await es.getAuth().login(config.user, config.password);

        // Create the context (with group)
        const group = await es.createGroupAndContext();
        const homeContextID = group.getResource(true).getHomeContext();
        const contextEntry = await es.getEntry(es.getEntryURI('_contexts', homeContextID));

        const contextEntryInfo = contextEntry.getEntryInfo();

        const accessControl = contextEntryInfo.getACL(true);
        accessControl.mread.push(group.getId());
        contextEntryInfo.setACL(accessControl);

        // Set the type of the context to "Terms"
        contextEntryInfo.getGraph()
          .add(
            contextEntry.getResourceURI(),
            'rdf:type', 'esterms:TerminologyContext',
          );

        await contextEntryInfo.commit();


        // Create the concept scheme
        const conceptSchemeURI = importedGraph.find(null, 'rdf:type', 'skos:ConceptScheme')[0].getSubject();
        const contextResource = await contextEntry.getResource();

        // Pipe the graph through some filters
        const fixedGraph = processGraph({
          graph: importedGraph, filters, conceptSchemeURI, homeContextID });
        await createEntryInContext(contextResource, conceptSchemeURI, importedGraph);

        // Import all the terms
        let fixedGraphConcepts = fixedGraph.find(null, 'rdf:type', 'skos:Concept');
        fixedGraphConcepts = fixedGraphConcepts.filter(stmt =>
          includeURI(stmt.getSubject(), filters));

        updateConsoleLine('Importing concepts...');
        let count = 1;
        const total = fixedGraphConcepts.length;
        const newEntries = [];
        await promiseUtil.forEach(fixedGraphConcepts, (concept) => {
          const subjectURI = concept.getSubject();
          updateConsoleLine(`Importing concepts: ${((count / total) * 100).toFixed(2)}%`);
          count += 1;
          return createEntryInContext(contextResource, subjectURI, importedGraph).then((e) => {
            newEntries.push(e);
            return e;
          });
        });
        /* const newEntries = await Promise.all(
          fixedGraphConcepts
            .map(async (concept) => {
              const subjectURI = concept.getSubject();

              updateConsoleLine(`Importing concepts: ${((count / total) * 100).toFixed(2)}%`);
              count += 1;

              return createEntryInContext(contextResource, subjectURI, importedGraph);
            }),
        ); */

        message(`Imported ${newEntries.length} concepts from ${importFileName}`);
      } catch (e) {
        console.log('ERROR', e);
        message('There was an error while importing. Aborting...');
      }
    });
});
