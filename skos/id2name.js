define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'config',
  'dojo/node!fs',
], (globals, message, config, fs) => {
  let ctxtid;
  let output;
  if (process.argv.length > 3) {
    ctxtid = process.argv[3];
  }

  if (process.argv.length > 4) {
    output = process.argv[4];
  }

  if (ctxtid == null || output == null) {
    message('You need to provide a context id to look for concepts and an output file for saving the index');
  } else {
    const es = globals.entrystore;
    es.getAuth().login(config.user, config.password).then(() => {
      const context = es.getContextById(ctxtid);
      const id2Name = {};
      es.newSolrQuery().rdfType('skos:Concept').context(context).list()
        .forEach((concept) => {
          const ruri = concept.getResourceURI();
          id2Name[ruri] = concept.getMetadata().findFirstValue(ruri, 'skos:prefLabel');
        })
        .then(() => {
          const fd = fs.openSync(`../generic/skos/${output}`, 'w');
          fs.writeSync(fd, JSON.stringify(id2Name, null, '  '), 0, 'utf8');
        }, () => {
          process.exit();
        });
    });
  }
});
