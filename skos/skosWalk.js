define(['../utils/treeWalk'], treeWalk =>
  /**
   * @param params.onEach - a callback that will be called for each entry
   * @param params.context - the context to limit the traversal in
   */
  (params) => {
    params.forward = ['skos:narrower', 'skos:hasTopConcept'];
    params.backward = ['skos:broader', 'skos:topConceptOf'];
    params.type = 'skos:ConceptScheme';
    return treeWalk(params);
  });
