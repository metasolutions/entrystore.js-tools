define([
    'dojo/_base/lang',
    'config',
    'store-tools/utils/message',
    './gnUtils',
    'rdfjson/namespaces',
    'store-tools/utils/globals'
], function(lang, config, message, gnUtils, namespaces, globals) {
    var es = globals.entrystore;

    var ctxid;
    if (process.argv.length > 3) {
        ctxid = process.argv[3];
    }

    var countryId;
    if (process.argv.length > 4) {
        countryId = process.argv[4];
    }

    var level = "1";
    if (process.argv.length > 5) {
        level = process.argv[5];
        if (level !== "1" && level !== "2" && level !== "3") {
            message("Level argument must be either 1, 2 or 3");
            process.exit();
        }
    }

    var importHierarchy = true;
    if (process.argv.length > 6) {
        if (process.argv[6] !== "all" && process.argv[6] !== "leaves") {
            message("Import argument must be either \"all\" or \"leaves\"");
            process.exit();
        }
        importHierarchy = process.argv[6] === "all";
    }

    var count = 0;
    var report = function(obj) {
        message("Importing feature "+obj.name);
        count++;
    };

    var fl = function(levelCompare, nextF, obj) {
        if (level === levelCompare) {
            report(obj);
            return gnUtils.object2Entry(ctxid, obj);
        } else if (importHierarchy) {
            report(obj);
            return gnUtils.object2Entry(ctxid, obj).then(function() {
                return gnUtils.getChildrenOf(obj.geonameId, nextF);
            });
        } else {
            return gnUtils.getChildrenOf(obj.geonameId, nextF);
        }
    };

    if (ctxid == null || countryId == null) {
        message(["You need to provide more arguments:", "$> ./run importChildren contextId countryId [1|2|3] [all|leaves]", "The next to last parameter indicates if children or grandchildren should be imported, the last indicates if all features in the hierarchy should be imported or only the leaves, default is all."]);
    } else {
        gnUtils.prepareNamespaces();
        var nameProp = namespaces.expand("gn:name");
        es.getAuth().login(config.user, config.password).then(function() {
            gnUtils.getChildrenOf(countryId, lang.partial(fl, "1", lang.partial(fl, "2", function(obj) {
                report(obj);
                return gnUtils.object2Entry(ctxid, obj);
            }))).then(function() {
                message("Imported " + count + " features");
            });
        });
    }
});