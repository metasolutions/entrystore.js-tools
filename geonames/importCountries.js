define([
  'config',
  'store-tools/utils/message',
  './gnUtils',
  'store-tools/utils/rdf',
  'rdfjson/namespaces',
  'store-tools/utils/globals',
  'dojo/_base/array',
], (config, message, gnUtils, rdf, namespaces, globals, array) => {
  const es = globals.entrystore;

  let ctxid;
  if (process.argv.length > 3) {
    ctxid = process.argv[3];
  }
  if (ctxid == null) {
    message(' You need to provide context id to import countries');
  } else {
    gnUtils.prepareNamespaces();
    const nameProp = namespaces.expand('gn:name');
    es.getAuth().login(config.user, config.password).then(() => {
      const list = es.newSolrQuery().rdfType('gn:Feature').limit(1).list();
      list.getEntries(0).then(() => {
        if (list.getSize() != 0) {
          message("there are gn:feature's in entrystore, delete them before import");
          return;
        }
        const url = 'http://api.geonames.org/searchRDF?maxRows=1000&username=metasolutions&style=full&adminCode1=00';
        rdf.loadRDFFromURL(url).then((rdfGraph) => {
          let countryStmts = rdfGraph.find(null, 'gn:featureCode', 'gn:A.PCLI');
          const countryStmts2 = rdfGraph.find(null, 'gn:featureCode', 'gn:A.PCLD');
          countryStmts = countryStmts.concat(countryStmts2);
          if (countryStmts.length > 0) {
            array.forEach(countryStmts, (cntryStmt, idx) => {
              message(`Importing country (${idx}): ${rdfGraph.findFirstValue(cntryStmt.getSubject(), nameProp)}`);
              const cntryUri = cntryStmt.getSubject();
              return gnUtils.createGNFeatureEntry(rdfGraph, cntryUri, ctxid);
            });
          }
        });
      });
    });
  }
});
