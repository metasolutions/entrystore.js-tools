define([
  'config',
  'dojo/promise/all',
  'store-tools/utils/message',
  './gnUtils',
  'store-tools/utils/rdf',
  'rdfjson/namespaces',
  'store/promiseUtil',
  'store-tools/utils/globals',
  'dojo/_base/array',
], (config, all, message, gnUtils, rdf, namespaces, promiseUtil, globals, array) => {
  const es = globals.entrystore;

  const continentIds = [
    '6255146',
    '6255147',
    '6255148',
    '6255149',
    '6255150',
    '6255151',
    '6255152',
  ];

  const fetchCountries = function (context) {
    const countries = [];
    return es.newSolrQuery().rdfType('gn:Feature').context(context).list().forEach((countryEntry) => {
      countries.push(countryEntry);
    }).then(() => countries);
  };

  const fetchContinents = function () {
    return all(array.map(continentIds, id => rdf.loadRDFFromURL(`http://sws.geonames.org/${id}/about.rdf`)));
  };

  const importContinents = function (context) {
    return fetchContinents().then(continentGraphs => all(array.map(continentGraphs, (gr) => {
      const stmts = gr.find(null, 'rdf:type', 'gn:Feature');
      if (stmts.length === 1) {
        return gnUtils.createGNFeatureEntry(gr, stmts[0].getSubject(), context.getId());
      }
      return all();
    })));
  };

  const fetchChildren = function () {
    return all(array.map(continentIds, id => rdf.loadRDFFromURL(`http://sws.geonames.org/${id}/contains.rdf`)));
  };
  const mergeGraphs = function (graphs) {
    const first = graphs[0];
    for (let i = 1; i < graphs.length; i++) {
      first.addAll(graphs[i]);
    }
    return first;
  };

  let ctxid;
  if (process.argv.length > 3) {
    ctxid = process.argv[3];
  }
  if (ctxid == null) {
    message(' You need to provide context id to import countries');
  } else {
    gnUtils.prepareNamespaces();
    const nameProp = namespaces.expand('gn:name');
    es.getAuth().login(config.user, config.password).then(() => {
      const context = es.getContextById(ctxid);
      importContinents(context).then((continents) => {
        fetchChildren().then((continentChildren) => {
          const relations = mergeGraphs(continentChildren);
          fetchCountries(context).then((countries) => {
            console.log(`found ${countries.length} countries`);
            promiseUtil.forEach(countries, (country) => {
              const curi = country.getResourceURI();
              const stmts = relations.find(curi, 'gn:parentFeature');
              if (stmts.length > 0) {
                const md = country.getMetadata().add(curi, 'gn:parentFeature', stmts[0].getValue());
                return country.commitMetadata();
              }
              console.log(`No relation found for country with uri: ${curi}`);
              return all();
            });
          });
        });
      });
    });
  }
});
