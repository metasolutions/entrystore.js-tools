define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'config',
  './gnUtils',
], (globals, message, config, gnUtils) => {
  let ctxtid;
  if (process.argv.length > 3) {
    ctxtid = process.argv[3];
  }

  if (ctxtid == null) {
    message('You need to provide context id to import countries');
  } else {
    const es = globals.entrystore;
    es.getAuth().login(config.user, config.password).then(() => {
      const context = es.getContextById(ctxtid);
      gnUtils.prepareNamespaces();
      gnUtils.deleteAllGNFeature(context);
    });
  }
});
