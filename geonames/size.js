define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'config',
  './gnUtils',
], (globals, message, config, gnUtils) => {
  const es = globals.entrystore;
  es.getAuth().login(config.user, config.password).then(() => {
    gnUtils.prepareNamespaces();
    const list = es.newSolrQuery().rdfType('gn:Feature').limit(1).list();
    list.getEntries(0).then(() => {
      message(`number of countries ${list.getSize()}`);
    });
  });
});
