define([
  'dojo/_base/array',
  './countries',
  'rdfjson/namespaces',
  'store-tools/utils/globals',
  'rdfjson/utils',
  'store-tools/utils/rdf',
  'store/promiseUtil',
], (array, countries, namespaces, globals, utils, rdf, promiseUtil) => {
  let workbench;

  const alpha2Obj = {};
  array.forEach(countries, (c) => {
    alpha2Obj[c['alpha-2']] = c;
  });

  const addRdfsLabel = function (graph, countryURI) {
    const name = graph.findFirstValue(countryURI, 'gn:name');
    if (name) {
      graph.addL(countryURI, 'rdfs:label', name);
    }
  };

  const addAlpha = function (graph, countryURI) {
    const alpha2 = graph.findFirstValue(countryURI, 'gn:countryCode');
    const c = alpha2Obj[alpha2];
    if (c && c['alpha-3']) {
      graph.addL(countryURI, 'http://www.wikidata.org/prop/direct/P297', alpha2);
      graph.addL(countryURI, 'http://www.wikidata.org/prop/direct/P298', c['alpha-3']);
    }
  };

  workbench = {

        /**
         *
         * @param rdfGraph
         * @param countryURI
         * @param entrystore
         * @param ctxid
         * @returns {entryPromise}
         */
    createGNFeatureEntry(rdfGraph, countryURI, ctxid) {
      const cntryMetadataGraph = utils.extract(rdfGraph, countryURI);
      addRdfsLabel(cntryMetadataGraph, countryURI);
      addAlpha(cntryMetadataGraph, countryURI);
      const context = globals.entrystore.getContextById(ctxid);
      const pCntryEntry = context.newLink(countryURI);
      pCntryEntry.setMetadata(cntryMetadataGraph);
      return pCntryEntry.commit();
    },

        /**
         * @returns {successOrFailPromise}
         */
    deleteAllGNFeature(context) {
      const list = globals.entrystore.newSolrQuery().rdfType('gn:Feature').list();
      const esu = globals.entrystoreutil;
      return esu.removeAll(list);
    },

        /**
         * adds namespaces for further use
         */
    prepareNamespaces() {
      namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
      namespaces.add('adms', 'http://www.w3.org/ns/adms#');
      namespaces.add('foaf', 'http://xmlns.com/foaf/0.1/');
      namespaces.add('odrs', 'http://schema.theodi.org/odrs#');
            // addded for sample test data
      namespaces.add('dct', 'http://purl.org/dc/terms/');
      namespaces.add('gn', 'http://www.geonames.org/ontology#');
      namespaces.add('owl', 'http://www.w3.org/2002/07/owl#');
    },

    getChildrenOf(parentId, callbackPerChild) {
      const url = `http://api.geonames.org/childrenJSON?userName=metasolutions&lang=en&geonameId=${parentId}`;
      return rdf.loadJSON(url).then((result) => {
        if (!callbackPerChild) {
          return result.geonames;
        }
        return promiseUtil.forEach(result.geonames, callbackPerChild);
      });
    },

    object2Entry(ctxid, gno) {
      const featureURI = `http://sws.geonames.org/${gno.geonameId}/`;
      return rdf.loadRDFFromURL(`${featureURI}about.rdf`).then(featureGraph => workbench.createGNFeatureEntry(featureGraph, featureURI, ctxid));
    },
  };
  return workbench;
});
