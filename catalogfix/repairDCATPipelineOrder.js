define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'rdfjson/namespaces',
  'store/promiseUtil',
  'store/types',
  'config',
], (globals, message, namespaces, promiseUtil, types, config) => {
  namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
  const es = globals.entrystore;
  const esu = globals.entrystoreutil;

  es.getAuth().login(config.user, config.password).then(() => {
    const pEntries = [];
    return es.newSolrQuery().graphType(types.GT_PIPELINE).list()
      .forEach((pEntry) => {
        pEntries.push(pEntry);
      }).then(() => {
        promiseUtil.forEach(pEntries, pEntry => pEntry.getResource().then((r) => {
          const ts = ['empty', 'check', 'fetch', 'validate', 'merge'];
          const arr = [];
          ts.forEach((t) => {
            const id = r.getTransformForType(t);
            if (id) {
              arr.push(id);
            }
          });
          if (arr.length >= 3) {
            r.setOrderOfTransforms(arr);
            return r.commit();
          }
        })).then(() => {
          process.exit();
        });
      });
  });
});
