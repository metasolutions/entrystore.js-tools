define([
  'store-tools/utils/message',
  'store/EntryStore',
  'store/promiseUtil',
  'rdfjson/namespaces',
  'config',
], (message, EntryStore, promiseUtil, namespaces, config) => {
  namespaces.add('esterms', 'http://entryscape.com/terms/');

  let filter = '*';
  if (process.argv.length > 3) {
    filter = process.argv[3];
  }

  promiseUtil.forEach(config.instances, (inst) => {
    if (filter && inst.domain.indexOf(filter) === -1 && filter !== '*') {
      return Promise.resolve(true);
    }
    console.log(`Fixing distribution files repository: ${inst.domain}`);
    const es = new EntryStore(`https://${inst.domain}/store`);
    return es.getAuth().login(inst.username || 'admin', inst.password)
      .then(() => {
        // Start fixing files
        const documentEntries = [];
        const orphanedEntries = [];
        return es.newSolrQuery().rdfType('foaf:Document').forEach((documentEntry) => {
          if (documentEntry.getReferrers('dcat:accessURL').length > 0) {
            documentEntries.push(documentEntry);
          } else {
            orphanedEntries.push(documentEntry);
          }
        }).then(() => {
          const deleted = [];
          return promiseUtil.forEach(orphanedEntries, (entry) => {
            const ruri = entry.getResourceURI();
            const md = entry.getMetadata();
            deleted.push(`${ruri} : ${md.findFirstValue(ruri, 'dcterms:title') || entry.getId()}`);
            return entry.del();
          }).then(() => {
            if (deleted.length > 0) {
              message('Deleted the following orphaned files:', deleted);
            } else {
              message('Found no orphaned files to delete.');
            }
            const changed = [];
            return promiseUtil.forEach(documentEntries, (entry) => {
              const ruri = entry.getResourceURI();
              const md = entry.getMetadata();
              changed.push(`${ruri} : ${md.findFirstValue(ruri, 'dcterms:title') || entry.getId()}`);
              md.findAndRemove(ruri, 'rdf:type');
              md.add(ruri, 'rdf:type', 'esterms:File');
              return entry.commitMetadata();
            }).then(() => {
              if (changed.length > 0) {
                message('Changing the following files:', changed);
              } else {
                message('Found no files to change type for.');
              }
              return es.getAuth().logout();
            });
          });
        });
      }, (err) => {
        console.warn(`Could not sign in to ${inst.domain}`);
        console.warn(err);
      });
  }).then(() => {
    console.log('Through all instances');
  });
});
