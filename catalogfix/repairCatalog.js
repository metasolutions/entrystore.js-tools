define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'rdfjson/namespaces',
  'store/promiseUtil',
  'config',
], (globals, message, namespaces, promiseUtil, config) => {
  let ctxtid;
  if (process.argv.length > 3) {
    ctxtid = process.argv[3];
  }
  namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
  const es = globals.entrystore;
  const esu = globals.entrystoreutil;

  const context = es.getContextById(ctxtid);
  es.getAuth().login(config.user, config.password).then(() => {
    const dss = [];
    return es.newSolrQuery().context(context).rdfType('dcat:Dataset').list()
      .forEach((dsEntry) => {
        dss.push(dsEntry.getResourceURI());
      })
      .then(() => esu.getEntryByType('dcat:Catalog', context).then((catalogEntry) => {
        const md = catalogEntry.getMetadata();
        const res = catalogEntry.getResourceURI();
        const stmts = md.find(res, 'dcat:dataset');
        const cdss = stmts.map(stmt => stmt.getValue());
        let modified = false;
        cdss.forEach((ds) => {
          if (dss.indexOf(ds) === -1) {
            md.findAndRemove(res, 'dcat:dataset', ds);
            modified = true;
            console.log(`Removing: ${ds}`);
          }
        });
        dss.forEach((ds) => {
          if (cdss.indexOf(ds) === -1) {
            md.add(res, 'dcat:dataset', ds);
            modified = true;
            console.log(`Adding: ${ds}`);
          }
        });
        if (modified) {
          return catalogEntry.commitMetadata();
        }
        return undefined;
      }))
      .then(() => {
        process.exit();
      });
  });
});
