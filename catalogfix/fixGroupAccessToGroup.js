define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'rdfjson/namespaces',
  'store/promiseUtil',
  'store/types',
  'config',
], (globals, message, namespaces, promiseUtil, types, config) => {
  namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
  const es = globals.entrystore;

  es.getAuth().login(config.user, config.password).then(() => {
    const groupId2Entry = {};
    es.newSolrQuery().graphType(types.GT_GROUP).forEach((groupEntry) => {
      groupId2Entry[groupEntry.getId()] = groupEntry;
    }).then(() => {
      const groupEntries = Object.values(groupId2Entry);
      console.log(`${groupEntries.length} groups to fix`);
      let ind = 0;
      promiseUtil.forEach(groupEntries, (groupEntry) => {
        const ei = groupEntry.getEntryInfo();
        const acl = ei.getACL(true);
        acl.rwrite.filter(id => groupId2Entry[id]).forEach((id) => {
          if (acl.mread.indexOf(id) === -1) {
            acl.mread.push(id);
          }
        });
        ind += 1;
        console.log(`Fixing ${ind}`);
        ei.setACL(acl);
        return ei.commit();
      });
    });
  });
});
