define([
  'store-tools/utils/globals',
  'store-tools/utils/message',
  'rdfjson/namespaces',
  'store/promiseUtil',
  'store/types',
  'config',
], (globals, message, namespaces, promiseUtil, types, config) => {
  namespaces.add('dcat', 'http://www.w3.org/ns/dcat#');
  const es = globals.entrystore;

  es.getAuth().login(config.user, config.password).then(() => {
    const groupEntries = {};
    es.newSolrQuery().graphType(types.GT_GROUP).forEach((groupEntry) => {
      groupEntries[groupEntry.getId()] = true;
    }).then(() => {
      const contextEntries = [];
      es.newSolrQuery().graphType(types.GT_CONTEXT).forEach((contextEntry) => {
        contextEntries.push(contextEntry);
      }).then(() => {
        console.log(`${contextEntries.length} contexts to fix`);
        let ind = 0;
        promiseUtil.forEach(contextEntries, (contextEntry) => {
          const ei = contextEntry.getEntryInfo();
          const acl = ei.getACL(true);
          acl.rwrite.filter(id => groupEntries[id]).forEach((id) => {
            if (acl.mread.indexOf(id) === -1) {
              acl.mread.push(id);
            }
          });
          ind += 1;
          console.log(`Fixing ${ind}`);
          ei.setACL(acl);
          return ei.commit();
        });
      });
    });
  });
});
