define([
  'store-tools/utils/message',
  'store/EntryStore',
  'store/types',
  'store/promiseUtil',
  'rdfjson/namespaces',
  'config',
], (message, EntryStore, types, promiseUtil, namespaces, config) => {
  namespaces.add('esterms', 'http://entryscape.com/terms/');

  let filter = '*';
  if (process.argv.length > 3) {
    filter = process.argv[3];
  }

  promiseUtil.forEach(config.instances, (inst) => {
    if (filter && inst.domain.indexOf(filter) === -1 && filter !== '*') {
      return Promise.resolve(true);
    }
    console.log(`Fixing distribution files repository: ${inst.domain}`);
    const es = new EntryStore(`https://${inst.domain}/store`);
    return es.getAuth().login(inst.username || 'admin', inst.password)
      .then(() => {
        const ues = [];
        const userfixpromise = es.newSolrQuery().graphType(types.GT_USER).forEach((ue) => {
          ues.push(ue);
        }).then(() => promiseUtil.forEach(ues, (ue) => {
          const id = ue.getId();
          if (id === '_guest') {
            return Promise.resolve(true);
          }
          console.log(`Changing user: ${id}`);
          const ei = ue.getEntryInfo();
          const acl = ei.getACL(true);
          acl.mread = acl.mread.filter(eid => eid !== '_guest' && eid !== '_users');
          acl.rread = acl.rread.filter(eid => eid !== '_guest');
          acl.mread.push('_users');
          ei.setACL(acl);
          return ei.commit();
        }));

        const ges = [];
        const groupfixpromise = es.newSolrQuery().graphType(types.GT_GROUP).forEach((ge) => {
          ges.push(ge);
        }).then(() => promiseUtil.forEach(ges, (ge) => {
          const id = ge.getId();
          console.log(`Changing group: ${id}`);
          const ei = ge.getEntryInfo();
          const acl = ei.getACL(true);
          acl.mread = acl.mread.filter(eid => eid !== '_guest' && eid !== '_users' && eid !== id);
          acl.rread = acl.rread.filter(eid => eid !== '_guest' && eid !== id);
          acl.mwrite = acl.mwrite.filter(eid => eid !== '_admin');
          acl.rwrite = acl.rwrite.filter(eid => eid !== '_admin');
          acl.mread.push('_users');
          acl.rread.push(id);
          ei.setACL(acl);
          return ei.commit();
        }));
        return Promise.all([userfixpromise, groupfixpromise]);
      }, (err) => {
        console.warn(`Could not sign in to ${inst.domain}`);
        console.warn(err);
      });
  }).then(() => {
    console.log('Through all instances');
  });
});
