define([
  'store-tools/utils/message',
  'store/EntryStore',
  'store/types',
  'store/promiseUtil',
  'rdfjson/namespaces',
  'config',
], (message, EntryStore, types, promiseUtil, namespaces, config) => {
  namespaces.add('esterms', 'http://entryscape.com/terms/');
  namespaces.add('store', 'http://entrystore.org/terms/');

  const copy = (graph1, s1, graph2, s2, prop) => {
    graph1.find(s1, prop).forEach((stmt) => {
      if (stmt.getType() === 'literal') {
        graph2.addL(s2, prop, stmt.getValue(), stmt.getLanguage());
      } else {
        graph2.add(s2, prop, stmt.getValue());
      }
    });
  };
  promiseUtil.forEach(config.instances, (inst) => {
    console.log(`Fixing candidates in repository: ${inst.domain} and in context ${inst.context || '*'}`);
    const es = new EntryStore(`https://${inst.domain}/store`);
    return es.getAuth().login(inst.username || 'admin', inst.password)
      .then(async () => {
        const candidates = [];
        await es.newSolrQuery().context(inst.context).rdfType('esterms:CandidateDataset').forEach((candidate) => {
          candidates.push(candidate);
        });
        await promiseUtil.forEach(candidates, async (candidate) => {
          const typeStmt = candidate.getMetadata().find(null, 'rdf:type')[0];
          typeStmt.setValue(namespaces.expand('esterms:Suggestion'));
          // http://entryscape.com/terms/CandidateDataset
          // http://entryscape.com/terms/Suggestion
          console.log(`Changing candidate ${candidate.getId()} to suggestion`);
          await candidate.commitMetadata();
          const ei = candidate.getEntryInfo();
          ei.setStatus('http://entryscape.com/terms/investigating');
          await ei.commit();
        });

        const datasets = [];
        await es.newSolrQuery().context(inst.context).rdfType('dcat:Dataset').forEach((dataset) => {
          datasets.push(dataset);
        });

        await promiseUtil.forEach(datasets, async (dataset) => {
          console.log(`Found dataset ${dataset.getId()}`);
          const dmd = dataset.getMetadata();
          const dsubject = dataset.getResourceURI();
          const dei = dataset.getEntryInfo();
          const deiG = dei.getGraph();
          if (!deiG.findFirstValue(dsubject, 'store:progress')) {
            return;
          }
          const c = dataset.getContext();
          const ce = await c.getEntry();
          const group_ruri = (ce.getReferrers('store:homeContext') || [])[0];
          if (!group_ruri) {
            return;
          }
          console.log(`Creating suggestion for dataset ${dataset.getId()}`);
          const suggestion = c.newNamedEntry();
          const smd = suggestion.getMetadata();
          const ssubject = suggestion.getResourceURI();
          smd.add(ssubject, 'rdf:type', 'esterms:Suggestion');
          smd.add(ssubject, 'dcterms:references', dsubject);
          copy(dmd, dsubject, smd, ssubject, 'dcterms:title');
          copy(dmd, dsubject, smd, ssubject, 'dcterms:description');

          const sei = suggestion.getEntryInfo();
          sei.setStatus('http://entryscape.com/terms/investigating');
          const acl = sei.setACL({admin: [group_ruri]});

          const seiG = suggestion.getEntryInfo().getGraph();
          copy(deiG, dsubject, seiG, ssubject, 'store:progress');

          await suggestion.commit();
          deiG.findAndRemove(dsubject, 'store:progress');
          await dei.commit();
        });


      }, (err) => {
        console.warn(`Could not sign in to ${inst.domain}`);
        console.warn(err);
      });
  }).then(() => {
    console.log('Through all instances');
  });
});
